#!/bin/bash
# Config variables
INTERVAL=120
BLOCKS_MISSED_THRESHOLD=1
CHUNKS_MISSED_THRESHOLD=1


#POOL_ID="YOUR-POOL-pool.stake.guildnet"      #GUILDNET
#POOL_ID="YOUR-POOL.pool.f863973.m0"         #TESTNET
POOL_ID="YOUR-POOL.poolv1.near"                #MAINNET

#TG_BOT_ID="51...289:AAEhUdr3E4........FVf8IduX7yVrE"        #GUILDNET
#TG_BOT_ID="51...289:AAEhUdr3E4........FVf8IduX7yVrE"       #TESTNET
TG_BOT_ID="51...289:AAEhUdr3E4........FVf8IduX7yVrE"       #MAINNET

TG_URL="https://api.telegram.org/bot${TG_BOT_ID}/sendMessage"
TG_CHAT_ID="125....258"

#DISCORD_WEBHOOK="https://discord.com/api/webhooks/93811......597/cLQlIiYWiuQMkljbBuCbdiN8G........EmXFJbC3"        #GUILDNET
#DISCORD_WEBHOOK="https://discord.com/api/webhooks/93811......597/cLQlIiYWiuQMkljbBuCbdiN8G........EmXFJbC3"       #TESTNET
DISCORD_WEBHOOK="https://discord.com/api/webhooks/93811......597/cLQlIiYWiuQMkljbBuCbdiN8G........EmXFJbC3"       #MAINNET


# Internal variables
BLOCKS_WARNED=false
CHUNKS_WARNED=false
KICKED_OUT_WARNED=false
SYNCING_ALERT=false


function sendMsg() {
        MSG="$1"

        echo "$MSG"
        [[ ! -z $TG_BOT_ID ]] && curl -s -X POST ${TG_URL} -d chat_id="$TG_CHAT_ID" -d text="$MSG" > /dev/null 2>&1
        [[ ! -z $DISCORD_WEBHOOK ]] && curl -s -H "Accept: application/json" -H "Content-Type:application/json" -X POST --data "{\"content\": \"${MSG}\"}" ${DISCORD_WEBHOOK} > /dev/null 2>&1
}


sendMsg "🚦 Starting Near alerts..."

while :
do
        SYNCING=$(curl -s http://127.0.0.1:3030/status | jq .sync_info.syncing)

        if [ $SYNCING == "true" ] && [[ SYNCING_ALERT == false ]] ; then
                sendMsg "🔴 Pool $POOL_ID is not in sync (syncing = $SYNCING)"
		SYNCING_WARNED=true
	elif [ $SYNCING == "false" ] && [[ SYNCING_ALERT == true ]] ; then
                sendMsg "🟢 Pool $POOL_ID is in sync again (syncing = $SYNCING)"
		SYNCING_WARNED=false
        fi

        KICKED_OUT_REASON=$(curl -s -d '{"jsonrpc": "2.0", "method": "validators", "id": "dontcare", "params": [null]}' -H 'Content-Type: application/json' http://localhost:3030/ | jq -c ".result.prev_epoch_kickout[] | select(.account_id | contains (\"$POOL_ID\"))" | jq .reason)

        if [[ ! -z $KICKED_OUT_REASON ]] && [[ $KICKED_OUT_WARNED == false  ]]; then
                sendMsg "🔴 Pool $POOL_ID has been kicked out, reason: $KICKED_OUT_REASON"
		KICKED_OUT_WARNED=true
	elif [[ -z $KICKED_OUT_REASEON ]] && [[ $KICKED_OUT_WARNED == true ]] ; then
                sendMsg "🟢 Pool $POOL_ID is not kicked out"
		KICKED_OUT_WARNED=false
        fi


        VALIDATOR_CURRENT=$(curl -s -d '{"jsonrpc": "2.0", "method": "validators", "id": "dontcare", "params": [null]}' -H 'Content-Type: application/json' http://localhost:3030/ | jq -c ".result.current_validators[] | select(.account_id | contains (\"$POOL_ID\"))" | jq .)

        BLOCKS_PRODUCED=$(echo $VALIDATOR_CURRENT | jq .num_produced_blocks)
        BLOCKS_EXPECTED=$(echo $VALIDATOR_CURRENT | jq .num_expected_blocks)
	CHUNKS_PRODUCED=$(echo $VALIDATOR_CURRENT | jq .num_produced_chunks)
	CHUNKS_EXPECTED=$(echo $VALIDATOR_CURRENT | jq .num_expected_chunks)

        BLOCKS_MISSED=$(echo "scale=0 ; $BLOCKS_EXPECTED - $BLOCKS_PRODUCED / 1" | bc)
        BLOCKS_MISSED_PERCENTAGE=$(echo "scale=2 ; $BLOCKS_MISSED / $BLOCKS_EXPECTED * 100" | bc)
        CHUNKS_MISSED=$(echo "scale=0 ; $CHUNKS_EXPECTED - $CHUNKS_PRODUCED / 1" | bc)
        CHUNKS_MISSED_PERCENTAGE=$(echo "scale=2 ; $CHUNKS_MISSED / $CHUNKS_EXPECTED * 100" | bc)

        #sendMsg "Node $POOL_ID blocks, $BLOCKS_PRODUCED/$BLOCKS_EXPECTED ($BLOCKS_MISSED_PERCENTAGE%)"
	echo "Node $POOL_ID blocks, $BLOCKS_PRODUCED/$BLOCKS_EXPECTED ($BLOCKS_MISSED_PERCENTAGE%) - MISSED: $BLOCKS_MISSED ::: chunks $CHUNKS_PRODUCED/$CHUNKS_EXPECTED ($CHUNKS_MISSED_PERCENTAGE%) - MISSED: $CHUNKS_MISSED"


        if [[ "$BLOCKS_MISSED_PERCENTAGE" > "$BLOCKS_MISSED_THRESHOLD" ]] && [[ $BLOCKS_WARNED == false ]] ; then
                sendMsg "🟠 Pool $POOL_ID has missed too many blocks, $BLOCKS_PRODUCED/$BLOCKS_EXPECTED ($BLOCKS_MISSED_PERCENTAGE%)"
                BLOCKS_WARNED=true
        elif [[ "$BLOCKS_MISSED_PERCENTAGE" < "$BLOCKS_MISSED_THRESHOLD" ]] && [[ $BLOCKS_WARNED == true ]] ; then
                sendMsg "🟢 Pool $POOL_ID blocks back to normal rate, $BLOCKS_PRODUCED/$BLOCKS_EXPECTED ($BLOCKS_MISSED_PERCENTAGE%)"
                BLOCKS_WARNED=false
        fi

        if [[ "$CHUNKS_MISSED_PERCENTAGE" > "$CHUNKS_MISSED_THRESHOLD" ]] && [[ $CHUNKS_WARNED == false ]] ; then
                sendMsg "🟠 Pool $POOL_ID has missed too many chunks, $CHUNKS_PRODUCED/$CHUNKS_EXPECTED ($CHUNKS_MISSED_PERCENTAGE%)"
                CHUNKS_WARNED=true
        elif [[ "$CHUNKS_MISSED_PERCENTAGE" < "$CHUNKS_MISSED_THRESHOLD" ]] && [[ $CHUNKS_WARNED == true ]] ; then
                sendMsg "🟢 Pool $POOL_ID chunks back to normal rate, $CHUNKS_PRODUCED/$CHUNKS_EXPECTED ($CHUNKS_MISSED_PERCENTAGE%)"
                CHUNKS_WARNED=false
        fi


        sleep $INTERVAL
done
